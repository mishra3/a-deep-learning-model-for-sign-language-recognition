About The Project 

This is the Master's project developed by Shivam Mishra under guidance of Dr. Sargur N Srihari.
In this project Sign Language Detection is performed using AutoEncoder, LSTM and BiLSTM.

DataSet Used:- Argentian Sign Language Dataset 
@Article{Ronchetti2016,
author="Ronchetti, Franco and Quiroga, Facundo and Estrebou, Cesar and Lanzarini, Laura and Rosete, Alejandro",
title="LSA64: A Dataset of Argentinian Sign Language",
journal="XX II Congreso Argentino de Ciencias de la Computación (CACIC)",
year="2016"
}

Project Structure:- 

model folder contains all the architecture code of AutoEncoder, LSTM and BiLSTM

training folder contains training code of AutoEncoder, LSTM and BiLSTM

testing folder contains testing code of LSTM and Bilstm

utils folder contains preprocessing code and data partition code.

Results folder contains all the results of training and testing of LSTM and BiLSTM


Steps to conduct a Project 

1. Processing of data is already done and stored in training_bg_videos and testing_bg_videos.
2. To train the autoencoder run train_encoder notebook. After successfully completion of training for 150 epochs. Model will be saved in encoder folder.
3. After completion of autoencoder training, train the lstm from train_lstm notebook for 100 epochs. Model will be saved in lstm folder.
3. After completion of autoencoder training, train the bilstm from train_bilstm notebook for 100 epochs. Model will be saved in bilstm folder.
4. After successful completion of lstm and bilstm, test the lstm and bilstm on testing data.